FROM --platform=linux/arm64 node:16.14 As development

WORKDIR /usr/src/app

COPY package*.json ./

COPY tsconfig.build.json ./

COPY tsconfig.json ./

RUN npm config set maxsockets=5
RUN npm config set registry https://registry.npmjs.org/
RUN npm install --only=development

COPY . .

RUN npm run build

FROM --platform=linux/arm64 node:16.14 as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package*.json ./

COPY tsconfig.build.json ./

COPY tsconfig.json ./

RUN npm config set maxsockets=5
RUN npm config set registry https://registry.npmjs.org/
RUN npm install --only=production

COPY . .

COPY --from=development /usr/src/app/dist ./dist

CMD ["node", "dist/main"]
