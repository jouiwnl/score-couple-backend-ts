import { NextFunction, Request, Response } from "express";
import { auth } from '../firebase';

export class AuthMiddleware {
  validateUser(req: Request, res: Response, next: NextFunction) {
    const PATH_LOGIN = "/api/v1/auth/login";

    if (PATH_LOGIN === req.path) {
      return next();
    }

    if (!auth.currentUser) {
      return res.status(401).json({ statusCode: 401, message: 'Não Autorizado!' })
    }

    return next();
  }
}