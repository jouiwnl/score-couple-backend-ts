import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AuthMiddleware } from './middlewares/AuthMiddleware';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.setGlobalPrefix("/api/v1");
  app.use(new AuthMiddleware().validateUser);

  await app.listen(process.env.PORT || 3333);
}
bootstrap();
