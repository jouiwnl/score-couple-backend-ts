import { Body, Controller, Inject, Post } from "@nestjs/common";
import { signInWithEmailAndPassword, signOut } from "firebase/auth";
import { auth } from '../firebase.js';
import * as _ from 'lodash';
import { AuthService } from "src/services/auth.service";
import { Request, Response } from "express";

interface LoginProps {
  email: string;
  password: string;
}

@Controller("/auth")
export class AuthController {

  constructor(@Inject(AuthService) private readonly authService: AuthService) {}

  @Post("/login")
  async handleLogin(@Body() login: LoginProps): Promise<void | Error> {

    if (_.isEmpty(login.email) || _.isEmpty(login.password)) {
      throw new Error("Necessário informar os dois campos!");
    }

    try {
      signInWithEmailAndPassword(auth, login.email, login.password)
    } catch (e) {
      throw new Error(e);
    }
  }

  @Post("/logout")
  async handleSignOut(): Promise<void | Error> {
    try {
      signOut(auth);
    } catch (e) {
      throw new Error(e);
    }
  }

}